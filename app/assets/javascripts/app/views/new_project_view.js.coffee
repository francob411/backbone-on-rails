class App.Views.NewProjectView extends Backbone.View

  template: HandlebarsTemplates["app/templates/new_project"]

  events:
    'click [data-action="submit"]': "saveProject"

  render: ->
    @$el.html(@template(@model.toJSON()))
    @

  initialize: ->
    @listenTo @model, "sync", @render
    @listenTo @model, "invalid", @renderErrors    
    @listenTo @model, "error", @parseErrorResponse   
    @model.fetch() unless @model.isNew()



  saveProject: (e) ->
    e.preventDefault()
    @model.set name: @$('#name').val()
    @model.set description: @$('#description').val()
    @model.save {}, 
      success: -> App.Vent.trigger "project:create", @model
      error: -> App.Vent.trigger "error", @model

_.extend App.Views.NewProjectView.prototype, App.Mixins.Validatable