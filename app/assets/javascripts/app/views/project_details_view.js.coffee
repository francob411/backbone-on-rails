class App.Views.ProjectDetailsView extends Backbone.View

  template: HandlebarsTemplates['app/templates/project_details']

  events:
    'click [data-action="destroy"]' : "deleteProject"
    'click [data-action="edit"]' : "editProject"

  initialize: ->
    @childViews = []
    @model.fetch()
    @listenTo @model, "sync", @renderDetails

  render: ->
    @$el.html(@template(@model.toJSON()))
    @

  renderDetails: ->
    @$el.html(@template(@model.toJSON()))
    v = new App.Views.IssuesView({ collection: @model.issues }) 
    @childViews.push(v)
    @$("#issues").html(v.render().el)
       
  deleteProject: ->
    return unless confirm("Are you sure?")
    @model.destroy
      success: -> App.Vent.trigger "project:destroy"

  editProject: ->
    App.Vent.trigger "project:edit", @model