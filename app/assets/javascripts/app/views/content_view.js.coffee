class App.Views.ContentView extends Backbone.View
  
  className: "row"
    
  template: HandlebarsTemplates['app/templates/content']
  
  render: ->
    @$el.html(@template())
    @

  initialize: ->
    @listenTo App.Vent, "project:create", @swapMainWithProjectDetails
    @listenTo App.Vent, "project:new", @swapMainWithNewProject
    @listenTo App.Vent, "project:show", @swapMainWithProjectDetails
    @listenTo App.Vent, "project:destroy", @swapMainWithEmpty
    @listenTo App.Vent, "project:edit", @editProject

  editProject: (m) ->
    @swapMainView(new App.Views.NewProjectView({ model: m }))
    Backbone.history.navigate('/projects/edit/' + m.id) 

  swapMainWithNewProject: ->
    @swapMainView(new App.Views.NewProjectView({ model: new App.Models.Project }))
    Backbone.history.navigate("/projects/new")    

  swapMainWithEmpty: ->
    @swapMainView(new App.Views.EmptyView())
    Backbone.history.navigate("/projects")

  swapMainView: (v) ->
    @changeCurrentMainView(v)
    @$("#main-area").html(@currentMainView.render().el)

  swapMainWithProjectDetails: (m) ->
    @swapMainView(new App.Views.ProjectDetailsView({model:m}))
    Backbone.history.navigate('/projects/' + m.get("id"))
    
  changeCurrentMainView: (v) ->
    @currentMainView.leave() if @currentMainView
    @currentMainView = v

  swapSideView: (v) ->
    @changeCurrentSideView(v)
    @$("#sidebar-area").html(@currentSideView.render().el)

  changeCurrentSideView: (v) ->
    @currentSideView.leave() if @currentSideView
    @currentSideView = v  