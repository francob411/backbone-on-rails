class App.Views.IssuesView extends Backbone.View

  template: HandlebarsTemplates["app/templates/issues"]

  initialize: ->
    @childViews = []

  render: ->
    @$el.html(@template({count: @collection.length}))
    @collection.forEach @renderIssue, @
    @

  renderIssue: (model) ->
    v = new App.Views.IssueView({ model: model })
    @childViews.push(v)
    @$("#issues_list").append(v.render().el)

