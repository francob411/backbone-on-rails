#= require_self
#= require_tree ./templates
#= require_tree ./mixins
#= require_tree ./routers
#= require_tree ./views
#= require_tree ./models

window.App = 
  Routers: {}
  Views: {}
  Models: {}
  Collections: {}
  Mixins: {}
  Vent: _.clone(Backbone.Events)

  initialize: ->
    new App.Routers.MainRouter()
    Backbone.history.start()
