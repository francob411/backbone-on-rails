class App.Routers.MainRouter extends Backbone.Router
    
  routes:
    "": "index"
    "projects": "projects"
    "projects/new": "newProject"
    "projects/:id": "showProject"
    "projects/edit/:id": "editProject"

  initialize: ->
    @header = new App.Views.HeaderView()
    @content = new App.Views.ContentView()
    @footer = new App.Views.FooterView()

  renderLayout: ->
    $("#header").html(@header.render().el)
    $("#content").html(@content.render().el)
    $("#footer").html(@footer.render().el)

  index: ->
    @renderLayout()
    @content.swapMainView(new App.Views.AdsView())
    @content.swapSideView(new App.Views.NewsView()) 

  projects: ->
    @renderLayout()
    @content.swapMainView(new App.Views.EmptyView())
    @content.swapSideView(new App.Views.Projects({ collection: new App.Collections.Projects }))

  editProject: (id) ->
    @renderLayout()
    @content.swapSideView(new App.Views.Projects({ collection: new App.Collections.Projects }))
    m = new App.Models.Project({id: id})
    @content.swapMainView(new App.Views.NewProjectView({ model: m }))    

  newProject: ->
    @renderLayout()
    @content.swapMainView(new App.Views.NewProjectView({ model: new App.Models.Project }))
    @content.swapSideView(new App.Views.Projects({ collection: new App.Collections.Projects }))

  showProject: (id) ->
    @renderLayout()
    @content.swapSideView(new App.Views.Projects({ collection: new App.Collections.Projects }))
    m = new App.Models.Project({id: id})
    @content.swapMainView(new App.Views.ProjectDetailsView({ model: m }))    




  