App.Mixins.Validatable = 
  renderErrors: (model, errors) ->
    @removeErrors()
    _.each errors, @renderError, @

  renderError: (errors, attribute) ->
    err = errors.join ";"
    @$("#" + attribute).closest("div.control-group").addClass("error")
    @$("#" + attribute).closest("div.controls").append('<p class="help-inline">' + err + '</p>')

  removeErrors: ->
    @$("dov.control-group").removeClass("error")
    @$("p.help-inline").remove()

  parseErrorResponse: (model, response) ->
    if response and response.responseText
      errors = JSON.parse response.responseText
      @renderErrors(model, errors.errors)